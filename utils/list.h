#pragma once

#include <stddef.h>

#define __UID2(__a, __b)  __a ## __b
#define __UID1(__a, __b)  __UID2(__a, __b)
#define __UID  __UID1(__it__, __LINE__)

#define list_foreach(__l, __lp)                                                                     \
    __typeof__(__lp)UID;                                                                            \
    for (__lp = (__typeof__(__lp))((__l)->next); UID = __lp ? (__typeof__(__lp))(((struct list_node *)(__lp))->next) : NULL, __lp && ((struct list_node *)__lp != (__l)); __lp = UID)

#define list_foreach_noit(__l, __lp)                                                                \
    __typeof__(__lp)UID;                                                                            \
    for (__lp = (__l)->next; UID = __lp ? __lp->next : NULL, __lp && (__lp != (__l)); __lp = UID)

#define list_entry(__st,__sm,__val) \
    ((__st *)(((char *)(__val))-offsetof(__st,__sm)))

#define LIST_NODE_CONTAINER(__st,__sm,__val) \
    list_entry(__st,__sm,__val)

struct list_node {
    struct list_node *next;
    struct list_node *prev;
};

static inline int __attribute__ ((always_inline)) LIST_NODE_INIT(struct list_node *node)
{
    if (node == NULL) {
        return -1;
    }

    node->next = node;
    node->prev = node;

    return 0;
}

static inline int __attribute__ ((always_inline)) list_delete(struct list_node *node)
{
    if (node == NULL) {
        return -1;
    }

    if (node->prev == node) { 
        goto exit_list_delete;
    }

    if (node->prev == NULL) {
        goto exit_list_delete;
    }

    node->next->prev = node->prev;
    node->prev->next = node->next;

exit_list_delete:
    return LIST_NODE_INIT(node);
}

static inline int __attribute__ ((always_inline)) list_add(struct list_node *lst, struct list_node *node)
{
    if (lst == NULL || node == NULL) {
        return -1;
    }

    if (lst->prev == NULL || lst->next == NULL) {
        LIST_NODE_INIT(lst);
    }

    list_delete(node);

    node->prev = lst->prev;
    node->next = lst;
    node->next->prev = node;
    node->prev->next = node;

    return 0;
}

static inline int __attribute__ ((always_inline)) list_is_empty(struct list_node *list)
{
    if (list == NULL) {
        return -1;
    }

    if (list->next == NULL) {
        return 1;
    }

    if (list->next && (list->next == list)) {
        return 1;
    } else {
        return 0;
    }
}

static inline struct list_node * __attribute__ ((always_inline)) list_get_head(struct list_node *list)
{
    if (list == NULL) {
        return NULL;
    }

    if (list_is_empty(list)) {
        return NULL;
    }

    return list->next;
}

static inline struct list_node * __attribute__ ((always_inline)) list_pop_head(struct list_node *list)
{
    struct list_node *head = list_get_head(list);

    list_delete(head);

    return head;
}

static inline int __attribute__ ((always_inline)) list_get_tail(struct list_node *list, struct list_node **node)
{
    if (list == NULL || node == NULL) {
        return -1;
    }

    if (!list_is_empty(list)) {
        *node = list->prev;
        return 0;
    } else {
        return -1;
    }
}

static inline int __attribute__ ((always_inline)) list_size(struct list_node *list)
{
    int i = 0;
    if (list == NULL) {
        return -1;
    }

    struct list_node *node;
    list_foreach_noit(list, node) {
        i++;
    }

    return i;
}

/**
 * Callback type for list traversion.
 * @param node the node being traversed
 * @param cookie user context passed to the list_traverse function
 */
typedef void (*list_traverse_cb)(struct list_node *node, void *cookie);

static inline int __attribute__ ((always_inline)) list_traverse(struct list_node *list, list_traverse_cb cb, void *cookie)
{
    int i = 0;

    struct list_node *node = NULL;
    list_foreach_noit(list, node) {
        cb(node, cookie);
        i++;
    }

    return i;
}
