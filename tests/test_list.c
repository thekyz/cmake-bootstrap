#include <unity.h>

#include "../utils/list.h"

struct data_element {
    struct list_node node;
    int value;
};

struct data_element _dummy[] = {
    { .value = 0x1 },
    { .value = 0x2 },
    { .value = 0x3 },
    { .value = 0x4 },
    { .value = 0x5 },
    { .value = 0x6 },
};

int _list_cb_calist_count = 0;
static void _test_list_cb(struct list_node *node, void *cookie)
{
    struct data_element *data = (struct data_element *)node;
    (void)cookie;

    TEST_ASSERT_EQUAL(++_list_cb_calist_count, data->value);
}

static void test_list_traverse()
{
    struct list_node list = { 0, 0 };
    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        TEST_ASSERT_EQUAL(0, list_add(&list, &_dummy[it].node));
    }

    list_traverse(&list, _test_list_cb, NULL);
    TEST_ASSERT_EQUAL(_list_cb_calist_count, sizeof(_dummy)/sizeof(*_dummy));
}

static void test_list_foreach()
{
    struct list_node list = { 0, 0 };
    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        TEST_ASSERT_EQUAL(0, list_add(&list, &_dummy[it].node));
    }

    int index = 0;

    struct data_element *element;
    list_foreach(&list, element) {
        TEST_ASSERT_EQUAL(element->value, _dummy[index++].value);
    }

    TEST_ASSERT_EQUAL(index, sizeof(_dummy)/sizeof(*_dummy));
}

static void test_list_pop_node()
{
    struct list_node list = { 0, 0 };
    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        TEST_ASSERT_EQUAL(0, list_add(&list, &_dummy[it].node));
    }

    TEST_ASSERT_EQUAL(0, list_delete(&_dummy[4].node));
    TEST_ASSERT_EQUAL(5, list_size(&list));
    TEST_ASSERT_EQUAL(0, list_delete(&_dummy[3].node));
    TEST_ASSERT_EQUAL(4, list_size(&list));
    TEST_ASSERT_EQUAL(0, list_delete(&_dummy[2].node));
    TEST_ASSERT_EQUAL(3, list_size(&list));
    TEST_ASSERT_EQUAL(0, list_delete(&_dummy[5].node));
    TEST_ASSERT_EQUAL(2, list_size(&list));
    TEST_ASSERT_EQUAL(0, list_delete(&_dummy[1].node));
    TEST_ASSERT_EQUAL(1, list_size(&list));
    TEST_ASSERT_EQUAL(0, list_delete(&_dummy[0].node));
    TEST_ASSERT_EQUAL(0, list_size(&list));
}

static void test_list_delete_empty_list()
{
    struct list_node list = { 0, 0 };

    TEST_ASSERT_EQUAL(0, list_delete(&list));
}

static void test_list_pop_head_empty_list()
{
    struct list_node list = { 0, 0 };

    TEST_ASSERT_NULL(list_pop_head(&list));
}

static void test_list_pop_head_big_list()
{
    struct data_element dummy[] = {
        { .value = 1 },
        { .value = 2 },
        { .value = 3 },
        { .value = 4 },
        { .value = 5 },
        { .value = 6 },
        { .value = 7 },
        { .value = 8 },
        { .value = 9 },
        { .value = 10 },
        { .value = 11 },
        { .value = 12 },
        { .value = 13 },
        { .value = 14 },
        { .value = 14 },
        { .value = 15 },
        { .value = 16 },
        { .value = 17 },
        { .value = 18 },
        { .value = 19 },
        { .value = 20 },
    };

    struct list_node list = { 0, 0 };

    for (int it = 0; it < (int)(sizeof(dummy)/sizeof(*dummy)); it++) {
        TEST_ASSERT_EQUAL(0, list_add(&list, &dummy[it].node));
    }

    struct list_node *popped_node = NULL;
    size_t pop_count = 0;
    while ((popped_node = list_pop_head(&list))) {
        struct data_element *element = (struct data_element *)popped_node;
        TEST_ASSERT_EQUAL(element->value, dummy[pop_count].value);
        pop_count++;
    }

    TEST_ASSERT_EQUAL(pop_count, sizeof(dummy)/sizeof(*dummy));
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_list_traverse);
    RUN_TEST(test_list_foreach);
    RUN_TEST(test_list_pop_node);
    RUN_TEST(test_list_delete_empty_list);
    RUN_TEST(test_list_pop_head_empty_list);
    RUN_TEST(test_list_pop_head_big_list);
    return UNITY_END();
}


