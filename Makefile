.PHONY: build tests all clean

all: build

build: git_deps
	@mkdir -p build && cd build && cmake .. && make

tests:
	@build/tests/test_list

clean:
	@rm -rf build

git_deps:
	@git submodule update --init
