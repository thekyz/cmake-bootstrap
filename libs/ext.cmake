set(EXT_PREFIX libext)
set(EXT_REPO https://github.com/ext/ext)
set(EXT_VERSION 1.0.0)

ExternalProject_Add(
    ${EXT_PREFIX}
    PREFIX ${EXT_PREFIX}
    GIT_REPOSITORY ${EXT_REPO}
    GIT_TAG ${EXT_VERSION}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}/libs
)
